      SELECT player_name
           , SUM(phaser_shots) AS phaser_shots
           , SUM(kill_count) AS kill_count
           , TIMESTAMP(CONCAT(YEAR(insertion_date),'-',MONTH(insertion_date),'-01')) AS time_period
        FROM stats_player
        JOIN (SELECT stats_player_id
           , SUM(if(weapon = 'Phaser', shots, 0)) AS phaser_shots
              FROM stats_player_shots
              GROUP BY stats_player_id
             ) AS shots
       USING (stats_player_id)
       WHERE stats_player.is_robot = 0
       GROUP BY stats_player.player_name, time_period
       LIMIT 10;
