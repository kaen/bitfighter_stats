delimiter |
CREATE PROCEDURE prune_table()
BEGIN
   DECLARE old_table_name, new_table_name VARCHAR(20);
   DECLARE old_table_time, new_table_time DATE;
   SET old_table_time = ADDDATE(NOW(), INTERVAL -6 MONTH);
   SET old_table_name = CONCAT('player_', YEAR(old_table_time), '_', LPAD(MONTH(old_table_time), 2, '0'), '_mv');

   SET new_table_time = NOW();
   SET new_table_name = CONCAT('player_', YEAR(new_table_time), '_', LPAD(MONTH(new_table_time), 2, '0'), '_mv');

   SET @drop_statement_text = CONCAT("DROP TABLE IF EXISTS ", old_table_name, ";");

   SET @create_statement_text = CONCAT(
           "CREATE TABLE ", new_table_name,
           " ( player_id INT AUTO_INCREMENT PRIMARY KEY, player_name VARCHAR (255) UNIQUE KEY, phaser_shots INT NOT NULL, bouncer_shots INT NOT NULL, triple_shots INT NOT NULL, burst_shots INT NOT NULL, mine_shots INT NOT NULL, spybug_shots INT NOT NULL, phaser_shots_struck INT NOT NULL, bouncer_shots_struck INT NOT NULL, triple_shots_struck INT NOT NULL, burst_shots_struck INT NOT NULL, mine_shots_struck INT NOT NULL, spybug_shots_struck INT NOT NULL, win_count INT NOT NULL, lose_count INT NOT NULL, tie_count INT NOT NULL, dnf_count INT NOT NULL, points INT NOT NULL, kill_count INT NOT NULL, death_count INT NOT NULL, suicide_count INT NOT NULL, kill_death_ratio FLOAT NOT NULL, asteroid_crashes INT NOT NULL, flag_drops INT NOT NULL, flag_pickups INT NOT NULL, flag_returns INT NOT NULL, flag_scores INT NOT NULL, teleport_uses INT NOT NULL, switched_team_count INT NOT NULL, last_played TIMESTAMP NOT NULL);"
      );

   PREPARE drop_stmt FROM @drop_statement_text;
   EXECUTE drop_stmt;
   DEALLOCATE PREPARE drop_stmt;

   PREPARE create_stmt FROM @create_statement_text;
   EXECUTE create_stmt;
   DEALLOCATE PREPARE create_stmt;
   SELECT @drop_statement_text;
   SELECT @create_statement_text;
   SHOW TABLES;
END
|
CALL prune_table()
|
DROP PROCEDURE prune_table
|
