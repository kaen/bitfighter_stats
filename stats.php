<?php
$start_time = microtime(true);

require "db_functions.php";

/**
 * Pulls listed params into variables in the global namespace after sanitizing and setting
 * default values.
 * @param arr An associative array of form "param_name" => "default_value"
 */
function get_params($arr) {
  foreach ($arr as $key => $value) {
    global $$key;
    $$key = sanitize($_REQUEST[$key]);
    $$key = empty($$key) ? $value : $$key;
  }
}

/**
 * Send expiration header data and 304 if unmodified
 * @param last_modified modification time in seconds since the epoch
 */ 
function handle_cache_headers($last_modified) {
  header('Last-Modified:' .
  gmdate('D,d M Y H:i:s', $last_modified) . ' GMT');
  $request= getallheaders();
  if(isset($request['If-Modified-Since'])) {
    $modifiedSince = explode(';', $request['If-Modified-Since']);
    $modifiedSince = strtotime($modifiedSince[0]);
  }else {
    $modifiedSince= 0;
  }

  if($last_modified && $last_modified <= $modifiedSince) {
    header('HTTP/1.1 304 Not Modified');
    exit();
  }
}

function connect_to_db() {
	global $username;
	global $password;
	global $server;
	global $database;
	
	$connection  = mysql_pconnect($server, $username, $password) or die("Could not connect: \n" . mysql_error());
	mysql_select_db($database, $connection) or die("Cannot select db $dbname: \n" . mysql_error());
	return $connection;
}

/**
 * Create a link to this page with the current parameters, except where they are
 * overriden in $arr which is an associative array with parameter names and values
 */
function make_link($text, $arr) {
  if (!is_array($arr)) {
    $arr = array();
  }
  global $player;
  global $sort;
  global $order;
  global $page;
  global $year;
  global $month;
  global $alltime;
  global $authed;

  $base = explode("?", $_SERVER["REQUEST_URI"]);
  $result = '';

  $arr['player'] = !empty($arr['player']) ? $arr['player'] : $player;
  $arr['sort'] = !empty($arr['sort']) ? $arr['sort'] : $sort;
  $arr['order'] = !empty($arr['order']) ? $arr['order'] : $order;
  $arr['page'] = !empty($arr['page']) ? $arr['page'] : $page;
  $arr['year'] = !empty($arr['year']) ? $arr['year'] : $year;
  $arr['month'] = !empty($arr['month']) ? $arr['month'] : $month;
  $arr['alltime'] = !empty($arr['alltime']) ? $arr['alltime'] : $alltime;
  $arr['authed'] = !empty($arr['authed']) ? $arr['authed'] : $authed;

  if (!empty($arr['player'])) {
    $result .= 'player=' . $arr['player'] . '&';
  }
  if (!empty($arr['order'])) {
    $result .= 'order=' . $arr['order'] . '&';
  }
  if (!empty($arr['sort'])) {
    $result .= 'sort=' . $arr['sort'] . '&';
  }
  if (!empty($arr['page'])) {
    $result .= 'page=' . $arr['page'] . '&';
  }
  if (!empty($arr['authed'])) {
    $result .= 'authed=' . $arr['authed'] . '&';
  }

  // only add month and year if alltime was not specified
  if ($arr['alltime'] != 'yes') {
    if (!empty($arr['year'])) {
      $result .= 'year=' . $arr['year'] . '&';
    }
    if (!empty($arr['month'])) {
      $result .= 'month=' . $arr['month'];
    }
  } else {
    $result .= 'alltime=' . $arr['alltime'];
  }
  print("<a href=\"" . $base[0] . "?" . htmlspecialchars($result) . "\">$text</a>");
}

/**
 * Print a page selection div
 */
function make_pager () {
  global $page;
  global $pages;
  printf("<div class=\"pager\">Page $page of $pages<br/>");

  if ($page != 1) {
    make_link('first', array("page" => 1));
  } else {
    print 'first';
  }
  print("&nbsp;");

  if ($page > 1) {
    make_link('prev', array("page" => $page - 1));
  } else {
    print 'prev';
  }
  print("&nbsp;");

  if ($page < $pages) {
    make_link('next', array("page" => $page + 1));
  } else {
    print 'next';
  }
  print("&nbsp;");

  if ($page != $pages) {
    make_link('last', array("page" => $pages));
  } else {
    print 'last';
  }
  print("</div>");
}

function time_ago ($oldtime) {
  $secs = time() - strtotime($oldtime);
  $bit = array(
    '&nbsp;year'   => round($secs / 31556926),
    '&nbsp;month'  => $secs / 2592000 % 12,
    '&nbsp;week'   => $secs / 604800 % 52,
    '&nbsp;day'    => $secs / 86400 % 7,
    '&nbsp;hour'   => $secs / 3600 % 24,
    '&nbsp;minute' => $secs / 60 % 60,
    '&nbsp;second' => $secs % 60
  );

  foreach($bit as $unit => $value){
    if($value > 1) {
      $ret[] = $value . $unit . 's';
    } else if($value == 1) {
      $ret[] = $value . $unit;
    } else {
      continue;
    }
    break;
  }
  $ret[] = 'ago';

  return join('&nbsp;', $ret);
}

get_params(array(
  'player'  => '',
  'alltime' => '',
  'authed'  => '',
  'order'   => 'kill_death_ratio',
  'sort'    => 'DESC',
  'page'    => '1',
  'year'    => date('Y'),
  'month'   => date('m')
));

$per_page = 40;
$start = ($page - 1) * $per_page;

// Fields to display on pages and pretty column names for them
$displayed_fields = array(
    'player_name' => 'Player',
    'kill_death_ratio' => 'KDR',
    'kill_count' => 'Kills',
    'death_count' => 'Deaths',
    'suicide_count' => 'Suicides',
    'points' => 'Points',
    'win_count' => 'W',
    'lose_count' => 'L',
    'tie_count' => 'T',
    'game_count' => 'Total',
    'flag_drops' => 'Dropped',
    'flag_pickups' => 'Taken',
    'flag_returns' => 'Returned',
    'flag_scores' => 'Scored',
    'asteroid_crashes' => 'Asteroid&nbsp;Crashes',
    'teleport_uses' => 'Teleportations',
    'switched_team_count' => 'Team&nbsp;Switches',
    'last_played' => 'Last&nbsp;Seen',
);


// build filter
$filters = array();
if ($alltime != 'yes') {
  $filters[] = "time_period='$year-$month-01'";
}
if (!empty($player)) {
  $filters[] = "player_name LIKE '%$player%'";
}
if (!empty($authed)) {
  $filters[] = "is_authenticated=" . ($authed == 'yes' ? '1' : '0');
}
if (!empty($filters)) {
  $filter = "WHERE " . implode(" AND ", $filters);
}

// Construct query
if ($alltime == 'yes') {

  $stats_query = "
    SELECT player_name
      , SUM(kill_count) AS kill_count
      , SUM(death_count) AS death_count
      , SUM(kill_count) / SUM(death_count) AS kill_death_ratio
      , SUM(suicide_count) AS suicide_count
      , SUM(points) AS points
      , SUM(win_count) AS win_count
      , SUM(lose_count) AS lose_count
      , SUM(tie_count) AS tie_count
      , SUM(win_count) + SUM(lose_count) + SUM(tie_count) AS game_count
      , SUM(flag_drops) AS flag_drops
      , SUM(flag_pickups) AS flag_pickups
      , SUM(flag_returns) AS flag_returns
      , SUM(flag_scores) AS flag_scores
      , SUM(asteroid_crashes) AS asteroid_crashes
      , SUM(teleport_uses) AS teleport_uses
      , SUM(switched_team_count) AS switched_team_count
      , MAX(last_played) AS last_played
      , is_authenticated
    FROM player_mv
    $filter
    GROUP BY player_name, is_authenticated
    ORDER BY
      $order
      $sort
    LIMIT $start,$per_page
    ;
  ";

  // should only need to check for the current month
  // (as long as one game has been played)
  $last_modified_query = "
    SELECT
      MAX(last_played) AS last_update
    FROM player_mv
    WHERE time_period = '" . date("Y-m-01") ."'
    ;
  ";

  // huge and slow, I'd rather disable page counting for all-time views
  $count_query = "
    SELECT
      COUNT(counts.subcount) AS count
    FROM (
      SELECT COUNT(*) AS subcount
      FROM player_mv
      $filter
      GROUP BY player_name, is_authenticated
    ) AS counts
    ;
  ";

} else {

  $stats_query = "
    SELECT player_name
      , kill_count
      , death_count
      , kill_death_ratio
      , suicide_count
      , points
      , win_count
      , lose_count
      , tie_count
      , win_count + lose_count + tie_count AS game_count
      , flag_drops
      , flag_pickups
      , flag_returns
      , flag_scores
      , asteroid_crashes
      , teleport_uses
      , switched_team_count
      , last_played
      , is_authenticated
    FROM player_mv
    $filter
    ORDER BY
      $order
      $sort
    LIMIT $start,$per_page
    ;
  ";

  $last_modified_query = "
    SELECT
      MAX(last_played) AS last_update
    FROM player_mv
    WHERE time_period = '$year-$month-01'
    ;
  ";

  $count_query = "
    SELECT
      COUNT(*) AS count
    FROM player_mv
    $filter
    ;
  ";
}

// check last modification time against cache headers
connect_to_db();
$result = mysql_query($last_modified_query);
$result or die(mysql_error());
$row = mysql_fetch_assoc($result);
$last_modified = strtotime($row['last_update']);
handle_cache_headers($last_modified);

// count number of results
$result = mysql_query($count_query);
$result or die(mysql_error());
$row = mysql_fetch_assoc($result);
$count = $row['count'];
$pages = ceil($count / $per_page);

// run the actual player data query
$result = mysql_query($stats_query);
$result or die(mysql_error());
$playerdata = mysql_fetch_assoc($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>Bitfighter Player Stats</title>
<link rel='stylesheet' href='player_stats.css'></link>
<script type='text/javascript'>
function humane_date(date_str){
  var time_formats = [
    [60, 'Just Now'],
    [90, '1 Minute'], // 60*1.5
    [3600, 'Minutes', 60], // 60*60, 60
    [5400, '1 Hour'], // 60*60*1.5
    [86400, 'Hours', 3600], // 60*60*24, 60*60
    [129600, '1 Day'], // 60*60*24*1.5
    [604800, 'Days', 86400], // 60*60*24*7, 60*60*24
    [907200, '1 Week'], // 60*60*24*7*1.5
    [2628000, 'Weeks', 604800], // 60*60*24*(365/12), 60*60*24*7
    [3942000, '1 Month'], // 60*60*24*(365/12)*1.5
    [31536000, 'Months', 2628000], // 60*60*24*365, 60*60*24*(365/12)
    [47304000, '1 Year'], // 60*60*24*365*1.5
    [3153600000, 'Years', 31536000], // 60*60*24*365*100, 60*60*24*365
    [4730400000, '1 Century'], // 60*60*24*365*100*1.5
    ];

  var time = ('' + date_str).replace(/-/g,"/").replace(/[TZ]/g," ")
    , dt = new Date
    , seconds = ((dt - new Date(time) + (dt.getTimezoneOffset() * 60000)) / 1000)
    , token = ' Ago'
    , i = 0
    , format
    ;

  if (seconds < 0) {
    seconds = Math.abs(seconds);
    token = '';
  }

  while (format = time_formats[i++]) {
    if (seconds < format[0]) {
      if (format.length == 2) {
        return format[1] + (i > 1 ? token : ''); // Conditional so we don't return Just Now Ago
      } else {
        return Math.round(seconds / format[2]) + ' ' + format[1] + (i > 1 ? token : '');
      }
    }
  }

  // overflow for centuries
  if(seconds > 4730400000)
    return Math.round(seconds / 4730400000) + ' Centuries' + token;

  return date_str;
};

window.onload = function() {
  // hide advanced search
  document.getElementById('advanced-search').style.display = "none";

  // add js-related elements
  var advancedSearchLink = document.createElement('a');
  advancedSearchLink.innerHTML = 'Show advanced search options';
  advancedSearchLink.href = '#';
  advancedSearchLink.onclick = function () {
    var element = document.getElementById('advanced-search');
    if (element.style.display == 'none') {
      advancedSearchLink.innerHTML = 'Hide advanced search options';
      element.style.display = 'block';
    } else {
      advancedSearchLink.innerHTML = 'Show advanced search options';
      element.style.display = 'none';
    }
    return false;
  };
  document.getElementById('search-form').appendChild(advancedSearchLink);

  var offsetFromGMT = <?php print date('Z'); ?>;
  var cells = document.getElementsByClassName('last-seen');
  var i;
  var cellDate;
  for (i = 0; i < cells.length; i++) {
    cellDate = new Date(cells[i].innerHTML);
    cellDate.setTime(cellDate.getTime() + offsetFromGMT * 60 * 60 * 1000);
    cells[i].innerHTML = humane_date(cellDate.toISOString());
  }
};
</script>
</head>
<body>

<?php
make_pager();
?>

<form id='search-form' action="stats.php" method="get">
  <div>
    <div id="player-count"><?php print $count ?> result<?php print ($count != 1 ? 's' : '') ?> found</div>
    <label>Player name contains: </label><input type="text" name="player" value="<?php print $player; ?>"></input>
    <input type="submit" value="Search"></input>
    <div id="advanced-search">
    <fieldset>
    <legend>Authentication</legend>
    <input type="radio" name="authed" value="yes" <?php print $authed=='yes' ? 'checked' : '' ?>>Authenticated players</input><br/>
    <input type="radio" name="authed" value="no" <?php print $authed=='no' ? 'checked' : '' ?>>Unauthenticated players</input><br/>
    <input type="radio" name="authed" value="" <?php print empty($authed) ? 'checked' : '' ?>>Both</input>
    </fieldset>
    <input type="hidden" name="alltime" value="<?php print $alltime ?>"></input>
    <input type="hidden" name="year" value="<?php print $year ?>"></input>
    <input type="hidden" name="month" value="<?php print $month ?>"></input>
    </div>
  </div>
</form>
<div id="month-select">
<?php

// convert to months since 0 AD to iterate through them
$current_months_total = date('Y') * 12 + date('m');
if ($alltime == 'yes') {
  print '<span>All Time</span>';
} else {
  make_link("All Time", array("alltime" => yes));
}
for ($i = 5; $i >= 0; $i--) {
  $link_months_total = $current_months_total - $i;
  $link_month = $link_months_total % 12;
  $link_year = ($link_months_total - $link_month) / 12;
  $link_month_name = date("F", mktime(0, 0, 0, $link_month, 1));

  if ($link_month == $month && $link_year == $year && $alltime != 'yes') {
    print '<span>' . $link_month_name . ' ' . $link_year . ' </span>';
  } else {
    make_link($link_month_name . " " . $link_year . " ", array("month" => $link_month, "year" => $link_year, "alltime" => 'no', 'page' => '1'));
  }
}

?>
</div>
<?php
if (mysql_num_rows($result) > 0):
?>
<table>
  <tr class='table-sections'>
  <td colspan='6'>General</td>
  <td colspan='5'>Games</td>
  <td colspan='4'>Flags</td>
  <td colspan='4'>Misc</td>
  </tr>
  <tr class="table-head"><td>#</td>
<?php
  // Make header
  $sortchar = ($sort == 'DESC' ? '&#9660;' : '&#9650;');
  foreach($displayed_fields as $key => $value) {
    $active = $key == $order;
    if ($active) {
      $link_sort = $sort == 'DESC' ? 'ASC' : 'DESC';
      $header_class = 'active';
    } else {
      $link_sort = 'DESC';
    }
    print "<td id=\"$key\" class=\"$header_class\">";
    make_link($value . ($active ? $sortchar : '') , array("order" => $key, "page" => 1, "sort" => $link_sort));
    print "</td>\n";
  }
  print "</tr>";

  // Fill table body
  $i = $start + 1;
  $parity = true;
  while ($playerdata) {

    // Row parity
    print "<tr class=\"" . ($parity ? 'odd' : 'even') . "\">\n";
    $parity = !$parity;

    // Rank column
    print "\t<td>$i</td>\n";
    $i += 1;

    // Iterate through fields to be displayed and find the values in the current row
    foreach(array_keys($displayed_fields) as $field) {
      // Left-align player names
      $cell_class = ($field == 'player_name' ? 'left' : '');
      $cell_class = ($field == 'last_played' ? 'last-seen' : $cell_class);

      print "\t<td class='$cell_class'>";

      if ($field == 'kill_death_ratio') {
        // KDR to two decimal places
        printf("%.2f",$playerdata[$field]);

      } else if ($field == 'player_name') {
        // Make link to detailed player stats
        $link_class = $playerdata['is_authenticated'] ? 'auth' : '';
        print "<a class='$link_class' href=\"player.php?player=$playerdata[$field]\">" . str_replace(" ", "&nbsp;", $playerdata[$field]) . "</a>";

      } else {
        // Otherwise, just print
        print $playerdata[$field];
      }
      print "</td>\n";
    }
    print "</tr>\n";
    $playerdata = mysql_fetch_assoc($result);
  }
  print "</table>";

else:
  print "<p>No results to display</p>";

endif;

make_pager();
printf("<!--Generated in %ss at %s -->", round(microtime(true) - $start_time, 6), date('H:i:s'));
?>
</body>
</html>
